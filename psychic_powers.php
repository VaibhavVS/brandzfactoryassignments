<?php 
// Function that checks if 
// the binary string contains  
// 6 consecutive 1's or 0's 
function check($s, $m) 
{ 
    // length of binary  
    // string 
    $l = strlen($s); 
    if ($l>100) {
        echo 'Input string is too long';
    }
    // counts zeros 
    $c1 = 0; 
  
    // counts 1's 
    $c2 = 0; 
  
    for ($i = 0; $i <= $l; $i++)  
    { 
  
        if ($s[$i] == '0')  
        { 
            $c2 = 0; 
              
            // count consecutive  
            // 0's 
            $c1++;  
        } 
        else 
        { 
            $c1 = 0; 
  
            // count consecutive 1's 
            $c2++;  
        } 
        if ($c1 == $m or 
            $c2 == $m) 
            return true; 
    } 
    return false; 
} 
  
// Driver Code 
$s = "0001111110"; 
$m = 6; 
  
// function call 
if (check($s, $m)) 
    echo "Sorry, sorry!";  
else
    echo "Good luck!";
  
?> 